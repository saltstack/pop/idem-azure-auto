import copy
from collections import ChainMap

from tests.unit.idem_azure.states.azure import mock_utils


async def test_present_resource_not_exists_create_only_request_mocks(
    hub,
    ctx,
    resource_id_template,
    idem_resource_name,
    resource_type,
    resource_id_properties,
    resource_parameters_raw,
    resource_parameters_present,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_successful_put(
        hub,
        ctx,
        expected_resource_id=expected_resource_id,
        resource_type=resource_type,
        resource_parameters_raw=resource_parameters_raw,
    )
    mock_utils.mock_get_empty(
        hub,
        ctx=ctx,
        expected_resource_id=expected_resource_id,
        resource_type=resource_type,
    )

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await getattr(hub.states.azure, f"{resource_type}.present")(
        test_ctx,
        name=idem_resource_name,
        **resource_id_properties,
        **resource_parameters_present,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.would_create_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters_present,
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )

    ret = await getattr(hub.states.azure, f"{resource_type}.present")(
        ctx,
        name=idem_resource_name,
        **resource_id_properties,
        **resource_parameters_present,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.create_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters_present,
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )


async def test_present_resource_exists_update_only_request_mocks(
    hub,
    ctx,
    resource_id_template,
    idem_resource_name,
    resource_type,
    resource_id_properties,
    resource_parameters_raw,
    resource_parameters_present,
    resource_parameters_update_raw,
    resource_parameters_update_present,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_return_resource(
        hub,
        resource_parameters_raw,
        ctx=ctx,
        expected_resource_id=expected_resource_id,
        resource_type=resource_type,
    )
    mock_utils.mock_successful_put(
        hub,
        ctx,
        expected_resource_id=expected_resource_id,
        resource_type=resource_type,
        resource_parameters_raw=resource_parameters_raw,
        update_params_raw=resource_parameters_update_raw,
    )

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await getattr(hub.states.azure, f"{resource_type}.present")(
        test_ctx,
        name=idem_resource_name,
        **resource_id_properties,
        **resource_parameters_update_present,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.would_update_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=resource_parameters_present,
        expected_new_state={
            **resource_parameters_present,
            **resource_parameters_update_present,
        },
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )

    ret = await getattr(hub.states.azure, f"{resource_type}.present")(
        ctx,
        name=idem_resource_name,
        **resource_id_properties,
        **resource_parameters_update_present,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.update_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=resource_parameters_present,
        expected_new_state={
            **resource_parameters_present,
            **resource_parameters_update_present,
        },
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )


async def test_absent_resource_not_exists_only_request_mocks(
    hub,
    ctx,
    resource_id_template,
    idem_resource_name,
    resource_type,
    resource_id_properties,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)
    mock_utils.mock_get_empty(hub, ctx, expected_resource_id, resource_type)
    mock_utils.mock_error_delete(hub)

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await getattr(hub.states.azure, f"{resource_type}.absent")(
        test_ctx, name=idem_resource_name, **resource_id_properties
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        hub.tool.azure.comment_utils.already_absent_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )

    ret = await getattr(hub.states.azure, f"{resource_type}.absent")(
        ctx, name=idem_resource_name, **resource_id_properties
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        hub.tool.azure.comment_utils.already_absent_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )


async def test_absent_resource_exists_only_request_mocks(
    hub,
    ctx,
    resource_id_template,
    idem_resource_name,
    resource_type,
    resource_id_properties,
    resource_parameters_raw,
    resource_parameters_present,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)
    mock_utils.mock_get_return_resource(
        hub, resource_parameters_raw, ctx, expected_resource_id, resource_type
    )
    mock_utils.mock_successful_delete(
        hub,
        expected_return={},
        ctx=ctx,
        expected_resource_id=expected_resource_id,
        resource_type=resource_type,
    )

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await getattr(hub.states.azure, f"{resource_type}.absent")(
        test_ctx, name=idem_resource_name, **resource_id_properties
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"] is None
    assert (
        hub.tool.azure.comment_utils.would_delete_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )

    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=resource_parameters_present,
        expected_new_state=None,
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )

    ret = await getattr(hub.states.azure, f"{resource_type}.absent")(
        ctx, name=idem_resource_name, **resource_id_properties
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.delete_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )

    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=resource_parameters_present,
        expected_new_state=None,
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )


async def test_describe_only_request_mocks(
    hub,
    ctx,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_raw,
    resource_parameters_present,
    list_url_format,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    resource_list_raw = [
        {
            **resource_parameters_raw,
            "id": expected_resource_id,
        }
    ]

    api_version = hub.tool.azure.api_versions.get_api_version(resource_type)
    list_url = list_url_format.format(
        ctx=ctx, api_version=api_version, **resource_id_properties
    )

    mock_utils.mock_successful_list(hub, resource_list_raw, list_url)

    ret = await getattr(hub.states.azure, f"{resource_type}.describe")(ctx)

    assert len(ret) == 1
    assert expected_resource_id in ret.keys()
    ret_value = ret.get(expected_resource_id)
    assert len(ret_value) == 1
    assert f"azure.{resource_type}.present" in ret_value

    described_resource = ret_value.get(f"azure.{resource_type}.present")
    described_resource_map = dict(ChainMap(*described_resource))
    hub.tool.azure.test_utils.check_returned_states(
        actual_state=described_resource_map,
        expected_state=resource_parameters_present,
        resource_id_properties=resource_id_properties,
        name=expected_resource_id,
        resource_id=expected_resource_id,
    )


# It is recommended to use the other methods which mock only the get/put/delete requests. Leaving this method here
# just in case some resources cannot be migrated to the other request-only mocks. This method should be removed in the future.
async def test_present_resource_not_exists_with_mocks(
    hub,
    mock_hub,
    ctx,
    resource_id_template,
    idem_resource_name,
    resource_type,
    resource_id_properties,
    resource_parameters_raw,
    resource_parameters_present,
):
    mock_utils.unmock_generic_present_creation(hub, mock_hub, resource_type)

    expected_resource_id = resource_id_template.format(**resource_id_properties)

    expected_put = {
        "ret": {
            "id": expected_resource_id,
            "name": idem_resource_name,
            **resource_parameters_raw,
        },
        "result": True,
        "status": 200,
        "comment": hub.tool.azure.comment_utils.would_create_comment(
            f"azure.{resource_type}", idem_resource_name
        ),
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        for prop_name, prop_val in resource_id_properties.items():
            assert prop_val in url
        return expected_put

    def _check_get_parameters(_ctx, url, success_codes):
        raise RuntimeError(f"Get should not be invoked in {resource_type} present!")

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    expected_api_version = hub.tool.azure.api_versions.get_api_version(resource_type)
    ctx = mock_utils.mock_wrapper_get_present_context(
        mock_hub,
        ctx,
        resource_type=resource_type,
        old_state=None,
        name=idem_resource_name,
        resource_id=expected_resource_id,
        api_version=expected_api_version,
    )
    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await getattr(mock_hub.states.azure, f"{resource_type}.present")(
        test_ctx,
        name=idem_resource_name,
        **resource_id_properties,
        **resource_parameters_present,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.would_create_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters_present,
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await getattr(mock_hub.states.azure, f"{resource_type}.present")(
        ctx,
        name=idem_resource_name,
        **resource_id_properties,
        **resource_parameters_present,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.create_comment(
            f"azure.{resource_type}", idem_resource_name
        )
        in ret["comment"]
    )
    hub.tool.azure.test_utils.check_both_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters_present,
        resource_id_properties=resource_id_properties,
        name=idem_resource_name,
        resource_id=expected_resource_id,
    )


def test_resource_id_not_passed_successful_create(
    hub,
    ctx,
    fake_acct_data,
    __test,
    __resource_id_flag,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_present,
    resource_parameters_raw,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_empty(hub, ctx, expected_resource_id, resource_type)

    mock_utils.mock_successful_put(
        hub, ctx, expected_resource_id, resource_type, resource_parameters_raw, __test
    )

    hub.tool.azure.test_utils.set_get_resource_only_with_resource_id_flag_on_hub(
        __resource_id_flag
    )

    idem_name = "idem_name"

    # resource_id not passed, flag on
    # -> we should create successfully
    ret = hub.tool.azure.test_utils.run_state_with_hub(
        resource_type=resource_type,
        present_state_properties={
            "name": idem_name,
            **resource_id_properties,
            **resource_parameters_present,
        },
        test=__test,
        acct_data=fake_acct_data,
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert ret["comment"] == [
            hub.tool.azure.comment_utils.would_create_comment(
                f"azure.{resource_type}", idem_name
            )
        ]
    else:
        assert ret["comment"] == [
            hub.tool.azure.comment_utils.create_comment(
                f"azure.{resource_type}", idem_name
            )
        ]

    assert ret["old_state"] is None

    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["new_state"],
        {
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_id_properties,
            **resource_parameters_present,
        },
        [],
    )


def test_get_resource_only_with_resource_id_passed_non_existent_resource_failure(
    hub,
    ctx,
    fake_acct_data,
    __test,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_present,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_empty(hub, ctx, expected_resource_id, resource_type)
    mock_utils.mock_error_put(
        hub,
        "Should not attempt to put as that would be a create operation and user wants update!",
    )

    hub.tool.azure.test_utils.set_get_resource_only_with_resource_id_flag_on_hub(True)

    idem_name = "idem_name"

    # resource_id passed, flag on
    # -> we want to update
    ret = hub.tool.azure.test_utils.run_state_with_hub(
        resource_type=resource_type,
        present_state_properties={
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_id_properties,
            **resource_parameters_present,
        },
        test=__test,
        acct_data=fake_acct_data,
    )

    assert not ret["result"]

    assert ret["comment"] == [
        hub.tool.azure.comment_utils.does_not_exist_comment(
            f"azure.{resource_type}", idem_name
        )
    ]

    assert ret["old_state"] is None
    assert ret["new_state"] is None


def test_update_with_resource_id(
    hub,
    ctx,
    fake_acct_data,
    __test: bool,
    __resource_id_flag: bool,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_present,
    resource_parameters_raw,
    resource_parameters_update_present,
    resource_parameters_update_raw,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_return_resource(
        hub,
        resource_parameters_raw,
        ctx,
        expected_resource_id,
        resource_type,
    )

    mock_utils.mock_successful_put(
        hub,
        ctx,
        expected_resource_id,
        resource_type,
        resource_parameters_raw,
        update_params_raw=resource_parameters_update_raw,
        __test=__test,
    )

    hub.tool.azure.test_utils.set_get_resource_only_with_resource_id_flag_on_hub(
        __resource_id_flag
    )

    idem_name = "idem_name"

    # resource_id passed, flag on
    # -> we want to update
    ret = hub.tool.azure.test_utils.run_state_with_hub(
        resource_type=resource_type,
        present_state_properties={
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_id_properties,
            **resource_parameters_update_present,
        },
        test=__test,
        acct_data=fake_acct_data,
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert ret["comment"] == [
            hub.tool.azure.comment_utils.would_update_comment(
                f"azure.{resource_type}", idem_name
            )
        ]
    else:
        assert ret["comment"] == [
            hub.tool.azure.comment_utils.update_comment(
                f"azure.{resource_type}", idem_name
            )
        ]

    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["old_state"],
        {
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_parameters_present,
        },
        [],
    )

    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["new_state"],
        {
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_parameters_update_present,
        },
        [],
    )


def test_update_without_resource_id(
    hub,
    ctx,
    fake_acct_data,
    __test: bool,
    __resource_id_flag: bool,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_present,
    resource_parameters_raw,
    resource_parameters_update_present,
    resource_parameters_update_raw,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_return_resource(
        hub,
        resource_parameters_raw,
        ctx,
        expected_resource_id,
        resource_type,
    )

    if __resource_id_flag:
        mock_utils.mock_error_put(
            hub, "Should not call put as resource_id is not provided"
        )
    else:
        mock_utils.mock_successful_put(
            hub,
            ctx,
            expected_resource_id,
            resource_type,
            resource_parameters_raw,
            update_params_raw=resource_parameters_update_raw,
            __test=__test,
        )

    hub.tool.azure.test_utils.set_get_resource_only_with_resource_id_flag_on_hub(
        __resource_id_flag
    )

    idem_name = "idem_name"

    # resource_id passed, flag on
    # -> we want to update
    ret = hub.tool.azure.test_utils.run_state_with_hub(
        resource_type=resource_type,
        present_state_properties={
            "name": idem_name,
            **resource_id_properties,
            **resource_parameters_update_present,
        },
        test=__test,
        acct_data=fake_acct_data,
    )

    if __resource_id_flag:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.azure.comment_utils.already_exists_comment(
                resource_type=f"azure.{resource_type}",
                name=idem_name,
            )
            in ret["comment"]
        )
        assert ret["new_state"] == ret["old_state"]
    else:
        assert ret["result"], ret["comment"]

        if __test:
            assert ret["comment"] == [
                hub.tool.azure.comment_utils.would_update_comment(
                    f"azure.{resource_type}", idem_name
                )
            ]
        else:
            assert ret["comment"] == [
                hub.tool.azure.comment_utils.update_comment(
                    f"azure.{resource_type}", idem_name
                )
            ]
        hub.tool.azure.test_utils.check_actual_includes_expected(
            ret["new_state"],
            {
                "name": idem_name,
                "resource_id": expected_resource_id,
                **resource_parameters_update_present,
            },
            [],
        )

    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["old_state"],
        {
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_parameters_present,
        },
        [],
    )


def test_empty_update_with_resource_id(
    hub,
    ctx,
    fake_acct_data,
    __test,
    __resource_id_flag: bool,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_present,
    resource_parameters_raw,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_return_resource(
        hub,
        resource_parameters_raw,
        ctx,
        expected_resource_id,
        resource_type,
    )

    mock_utils.mock_error_put(hub, "Should not call put as operation is empty update")

    hub.tool.azure.test_utils.set_get_resource_only_with_resource_id_flag_on_hub(
        __resource_id_flag
    )

    idem_name = "idem_name"

    # resource_id not passed, flag off
    # -> should discover the VM and update it
    ret = hub.tool.azure.test_utils.run_state_with_hub(
        resource_type=resource_type,
        present_state_properties={
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_id_properties,
            **resource_parameters_present,
        },
        test=__test,
        acct_data=fake_acct_data,
    )

    assert ret["result"], ret["comment"]

    assert (
        hub.tool.azure.comment_utils.no_property_to_be_updated_comment(
            f"azure.{resource_type}", idem_name
        )
        in ret["comment"]
    )

    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["old_state"],
        {
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_parameters_present,
        },
        [],
    )

    assert ret["new_state"] == ret["old_state"]


def test_empty_update_without_resource_id(
    hub,
    ctx,
    fake_acct_data,
    __test,
    __resource_id_flag: bool,
    resource_id_template,
    resource_type,
    resource_id_properties,
    resource_parameters_present,
    resource_parameters_raw,
):
    expected_resource_id = resource_id_template.format(**resource_id_properties)

    mock_utils.mock_get_return_resource(
        hub,
        resource_parameters_raw,
        ctx,
        expected_resource_id,
        resource_type,
    )

    mock_utils.mock_error_put(hub, "Should not call put as operation is empty update")

    hub.tool.azure.test_utils.set_get_resource_only_with_resource_id_flag_on_hub(
        __resource_id_flag
    )

    idem_name = "idem_name"

    # resource_id not passed, flag off
    # -> should discover the VM and update it
    ret = hub.tool.azure.test_utils.run_state_with_hub(
        resource_type=resource_type,
        present_state_properties={
            "name": idem_name,
            **resource_id_properties,
            **resource_parameters_present,
        },
        test=__test,
        acct_data=fake_acct_data,
    )

    if __resource_id_flag:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.azure.comment_utils.already_exists_comment(
                resource_type=f"azure.{resource_type}",
                name=idem_name,
            )
            in ret["comment"]
        )
    else:
        assert ret["result"], ret["comment"]

        assert (
            hub.tool.azure.comment_utils.no_property_to_be_updated_comment(
                f"azure.{resource_type}", idem_name
            )
            in ret["comment"]
        )

    hub.tool.azure.test_utils.check_actual_includes_expected(
        ret["old_state"],
        {
            "name": idem_name,
            "resource_id": expected_resource_id,
            **resource_parameters_present,
        },
        [],
    )

    assert ret["new_state"] == ret["old_state"]
