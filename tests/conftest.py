import pathlib

import pytest


@pytest.fixture(scope="session")
def code_dir() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent.absolute()
