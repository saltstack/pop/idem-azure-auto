import copy
from collections import ChainMap

import pytest


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE = "network.network_interfaces"
RESOURCE_ID_FORMAT = "/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/networkInterfaces/{network_interface_name}"

PROPERTIES = {
    "name": "name",
    "network_interface_name": "my-nic",
    "location": "eastus",
    "ip_configurations": [
        {
            "name": "test-ipc",
            "private_ip_address_allocation": "Static",
            "subnet_id": "subnet_resource_id",
            "private_ip_address_version": "IPv4",
            "private_ip_address": "available_ip_from_subnet",
            "primary": True,
        }
    ],
    "enable_accelerated_networking": True,
    "dns_settings": {
        # Azure DNS Virtual Server IP Address
        "dns_servers": ["168.63.129.16"],
        "internal_dns_name_label": "dns-name-label",
    },
    "network_security_group_id": "network_security_group_resource_id",
    "tags": {"tag-key": "tag-value"},
}

UPDATED_PROPERTIES = {
    "enable_accelerated_networking": False,
    "ip_configurations": [
        {
            "private_ip_address_allocation": "Dynamic",
        },
    ],
    "dns_settings": {
        "dns_servers": [],
        "internal_dns_name_label": "dns-name-label-updated",
    },
    "tags": {"tag-key-updated": "tag-value-updated"},
}


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_create")
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_network_interfaces_create(
    hub,
    ctx,
    idem_cli,
    __test,
    resource_group_fixture,
    subnet_fixture,
    network_security_group_fixture,
    function_cleaner,
    available_ip_fixture: str,
):
    """
    This test provisions a network interface, describes network interface and deletes network interface.
    """
    global PROPERTIES
    # Create network interface
    name = hub.tool.azure.test_utils.generate_unique_name("idem-test-network-interface")

    PROPERTIES["name"] = name
    PROPERTIES["network_interface_name"] = name
    PROPERTIES["subscription_id"] = ctx.acct.get("subscription_id")
    PROPERTIES["resource_group_name"] = resource_group_fixture.get("name")
    PROPERTIES["ip_configurations"][0]["subnet_id"] = subnet_fixture.get("id")
    PROPERTIES["network_security_group_id"] = network_security_group_fixture.get("id")
    PROPERTIES["ip_configurations"][0]["private_ip_address"] = available_ip_fixture

    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.create_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=name,
            )
            in ret["comment"]
        )

    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    expected_state = {"resource_id": resource_id, **PROPERTIES}
    assert ret["new_state"] == expected_state


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_create"])
async def test_network_interfaces_describe(hub, ctx):
    ret = await hub.states.azure.network.network_interfaces.describe(ctx)
    current_resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)
    for resource_id in ret:
        described_resource = ret[resource_id].get(f"azure.{RESOURCE_TYPE}.present")
        assert described_resource
        resource_state = dict(ChainMap(*described_resource))

        if resource_id == current_resource_id:
            expected_state = {"resource_id": resource_id, **PROPERTIES}
            expected_state["name"] = resource_id
            assert resource_state == expected_state
        else:
            assert resource_state.get("resource_id") == resource_id


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_network_interfaces_empty_update(hub, ctx, idem_cli, __test):
    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PROPERTIES, __test
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.azure.comment_utils.no_property_to_be_updated_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    assert ret["old_state"] == ret["new_state"]
    expected_new_state = {
        "resource_id": RESOURCE_ID_FORMAT.format(**PROPERTIES),
        **PROPERTIES,
    }
    assert ret["new_state"] == expected_new_state


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_update", depends=["present_create"])
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_network_interfaces_update(hub, ctx, idem_cli, __test):
    updated_state = copy.deepcopy(PROPERTIES)
    ip_config_props_to_update = UPDATED_PROPERTIES["ip_configurations"][0]

    ip_config_payload = copy.deepcopy(PROPERTIES["ip_configurations"][0])
    ip_config_payload.update(ip_config_props_to_update)

    UPDATED_PROPERTIES["ip_configurations"][0] = ip_config_payload
    updated_state.update(UPDATED_PROPERTIES)

    ret = hub.tool.azure.test_utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, updated_state, __test
    )

    assert ret["result"], ret["comment"]
    resource_id = RESOURCE_ID_FORMAT.format(**updated_state)
    expected_old_state = {"resource_id": resource_id, **PROPERTIES}
    assert ret["old_state"] == expected_old_state

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.update_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=updated_state["name"],
            )
            in ret["comment"]
        )
    expected_new_state = {"resource_id": resource_id, **updated_state}
    assert ret["new_state"] == expected_new_state


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["present_create"])
async def test_network_interfaces_absent(hub, ctx, idem_cli, __test):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx, idem_cli, RESOURCE_TYPE, PROPERTIES["name"], resource_id, test=__test
    )

    assert ret["result"], ret["comment"]
    current_state = copy.deepcopy(PROPERTIES)
    current_state.update(UPDATED_PROPERTIES)
    expected_old_state = {"resource_id": resource_id, **current_state}
    assert ret["old_state"] == expected_old_state
    assert not ret["new_state"]

    if __test:
        assert [
            hub.tool.azure.comment_utils.would_delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.azure.comment_utils.delete_comment(
                resource_type=f"azure.{RESOURCE_TYPE}",
                name=PROPERTIES["name"],
            )
            in ret["comment"]
        )
        ret = await hub.exec.azure.network.network_interfaces.get(
            ctx, resource_id=resource_id
        )
        assert ret["result"]
        assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_network_interfaces_already_absent(hub, ctx, idem_cli, __test):
    resource_id = RESOURCE_ID_FORMAT.format(**PROPERTIES)

    ret = await hub.tool.azure.test_utils.call_absent(
        ctx, idem_cli, RESOURCE_TYPE, PROPERTIES["name"], resource_id, test=__test
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]

    assert (
        hub.tool.azure.comment_utils.already_absent_comment(
            resource_type=f"azure.{RESOURCE_TYPE}",
            name=PROPERTIES["name"],
        )
        in ret["comment"]
    )
    ret = await hub.exec.azure.network.network_interfaces.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"]
    assert not ret["ret"]
