from typing import Any
from typing import Dict
from typing import List

import pytest


# List of the Azure VM power statuses
# defined in the Azure documentation -> https://learn.microsoft.com/en-us/azure/virtual-machines/states-billing
VM_STATUSES = [
    "creating",
    "starting",
    "running",
    "stopping",
    "stopped",
    "deallocating",
    "deallocated",
]


@pytest.fixture(scope="module")
async def vm_list_fixture(hub, ctx) -> List[Dict[str, Any]]:
    ret = await hub.exec.azure.compute.virtual_machines.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, vm_list_fixture):
    if len(vm_list_fixture) > 0:
        vm = vm_list_fixture[0]

        ret = await hub.exec.azure.compute.virtual_machines.get(
            ctx, resource_id=vm["resource_id"]
        )

        assert ret["result"], ret["comment"]
        assert ret["ret"], ret["comment"]

        vm_get_ret = ret["ret"]

        assert vm["resource_id"] == vm_get_ret["resource_id"]
        assert vm["location"] == vm_get_ret["location"]

        assert vm_get_ret["status"], "VM status is not available"
        assert vm_get_ret["status"] in VM_STATUSES, (
            f"VM status '{vm_get_ret['status']}' "
            f"is not part of the allowed azure statuses: {VM_STATUSES}"
        )
